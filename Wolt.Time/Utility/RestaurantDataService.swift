//
//  RestaurantDataService.swift
//  Wolt.Time
//
//  Created by Mikhail Bobretsov on 17/02/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import Foundation
import CSV
import CSVImporter

class RestaurantDataService {
    
    public static let shared = RestaurantDataService()
    
    func getRestaurantList(date: Date) -> [Restaurant]{
        var restaurantArray = [Restaurant]()
        let stream = InputStream(fileAtPath: Bundle.main.path(forResource: "locations", ofType: "csv")!)!
        do {
            let csv = try CSVReader(stream: stream, hasHeaderRow: true)
            while csv.next() != nil {
                if let idString = csv["location_id"], let id = Int(idString), let longitudeString = csv["longitude"], let longitude = Double(longitudeString), let latitudeString = csv["latitude"], let latitude = Double(latitudeString) {
                    let restaurant = Restaurant(id: id, longitude: longitude, latitude: latitude)
                    restaurantArray.append(restaurant)
                }
            }
        } catch _ {
            fatalError()
        }
        
        if !restaurantArray.isEmpty {
            
            let stream = InputStream(fileAtPath: Bundle.main.path(forResource: "pickup_times", ofType: "csv")!)!
            do {
                let csv = try CSVReader(stream: stream, hasHeaderRow: true)
                let dateFormatter = ISO8601DateFormatter()
                while csv.next() != nil {
                    if let csvDate = csv["iso_8601_timestamp"], let formattedCsvDate = dateFormatter.date(from: csvDate) {
                        if date.day == formattedCsvDate.day && date.month == formattedCsvDate.month && date.year == formattedCsvDate.year && date.hour == formattedCsvDate.hour {
                            if let idString = csv["location_id"], let id = Int(idString), let pickupTimeString = csv["pickup_time"], let pickupTime = Int(pickupTimeString) {
                                if let rest = restaurantArray.first(where: { (restaurant) -> Bool in
                                    return restaurant.id == id
                                }) { 
                                    rest.totalPickUpTime.append(pickupTime)
                                }
                                
                            }
                        }
                    }
                }
            } catch _ {
                fatalError()
            }
            
            for r in restaurantArray {
                if !r.totalPickUpTime.isEmpty {
                    r.averagePickUpTime = r.totalPickUpTime.reduce(0, +) / r.totalPickUpTime.count
                }
            }
        }
        
        return restaurantArray
    }
    
}
