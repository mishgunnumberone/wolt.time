//
//  File.swift
//  Wolt.Time
//
//  Created by Mikhail Bobretsov on 17/02/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import Foundation
import MapKit

class Restaurant {
    
    let id: Int
    let longitude: Double
    let latitude: Double
    
    var totalPickUpTime: [Int]
    var averagePickUpTime: Int?
    
    init(id: Int, longitude: Double, latitude: Double) {
        
        self.id = id
        self.longitude = longitude
        self.latitude = latitude
        self.totalPickUpTime = [Int]()
    }
    
}

class RestaurantAnnotation: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
