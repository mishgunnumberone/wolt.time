//
//  WoltTimeViewController.swift
//  Wolt.Time
//
//  Created by Mikhail Bobretsov on 14/02/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

class WoltTimeViewController: UIViewController {
    
    static let appColor = UIColor(red: 1.0/255.0, green: 18.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Wolt.Time"
        label.font = UIFont(name: "Roboto-Black", size: 49)
        label.textColor = WoltTimeViewController.appColor
        return label
    }()
    
    lazy var dateAndTimeContainer: DateAndTimeContainer = {
       let view = DateAndTimeContainer()
        view.translatesAutoresizingMaskIntoConstraints = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(dateAndTimeContainerTapped(sender:)))
        view.addGestureRecognizer(tap)
        return view
    }()
    
    lazy var clockImage: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "clock")
        return imageView
    }()
    
    lazy var dateAndTimeLabel: UILabel = {
       let label = UILabel()
        label.text = "Date and Time:"
        label.font = UIFont(name: "Roboto-Medium", size: 18)
        return label
    }()
    
    lazy var dateAndTimeInfoLabel: UILabel = {
        let label = UILabel()
        label.text = "Tap to select time.."
        label.font = UIFont(name: "Roboto-Regular", size: 16)
        return label
    }()
    
    lazy var timeMap: MKMapView = {
       let mapView = MKMapView()
        return mapView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        setInitialMapLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func initUI() {
        
        view.backgroundColor = UIColor.white
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(55)
            make.centerX.equalTo(view)
        }
        
        self.view.addSubview(dateAndTimeContainer)
        dateAndTimeContainer.addTopAndBottomBorders(height: 55)
        dateAndTimeContainer.snp.makeConstraints { (make) in
            make.height.equalTo(55)
            make.top.equalTo(titleLabel.snp.bottom).offset(35)
            make.left.right.equalTo(view)
        }
       
        dateAndTimeContainer.addSubview(clockImage)
        clockImage.snp.makeConstraints { (make) in
            make.height.width.equalTo(30)
            make.centerY.equalTo(dateAndTimeContainer)
            make.left.equalTo(dateAndTimeContainer).offset(8)
        }

        dateAndTimeContainer.addSubview(dateAndTimeLabel)
        dateAndTimeLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(dateAndTimeContainer)
            make.left.equalTo(clockImage.snp.right).offset(10)
        }
        
        dateAndTimeContainer.addSubview(dateAndTimeInfoLabel)
        dateAndTimeInfoLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(dateAndTimeContainer)
            make.right.equalTo(dateAndTimeContainer).offset(-8)
        }
        
        self.view.addSubview(timeMap)
        timeMap.snp.makeConstraints { (make) in
            make.right.bottom.left.equalTo(view)
            make.top.equalTo(dateAndTimeContainer.snp.bottom).offset(0)
        }
    }
    
    func setInitialMapLocation() {
        let initialLocation = CLLocation(latitude: 60.1699, longitude: 24.9384)
        let regionRadius: CLLocationDistance = 6000
        let coordinateRegion = MKCoordinateRegion(center: initialLocation.coordinate,
                                                      latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
            timeMap.setRegion(coordinateRegion, animated: true)
        
    }
    
    @objc func dateAndTimeContainerTapped(sender: UIView) {
        let alert = UIAlertController(title: "Date and Time", message: "Select date and time for orders", preferredStyle: .actionSheet)
        let pickerData = [["7.1.2019", "8.1.2019", "9.1.2019", "10.1.2019", "11.1.2019", "12.1.2019", "13.1.2019"],
                          ["0-1","1-2","2-3","3-4","4-5","5-6","6-7","7-8","8-9","9-10","10-11","11-12","12-13","13-14","14-15","15-16","17-18","18-19","19-20","20-21","21-22","22-23","23-24"]]
        var selectedPickerData: PickerViewViewController.Index?
        
        if dateAndTimeInfoLabel.text! == "Tap to select time.." {
            dateAndTimeInfoLabel.text = "\(pickerData[0][0]) \(pickerData[1][0])"
        } else {
            selectedPickerData = [[0,pickerData[0].index(of: String(dateAndTimeInfoLabel.text!.split(separator: " ").first!))!], [1,pickerData[1].index(of: String(dateAndTimeInfoLabel.text!.split(separator: " ").last!))!]]
        }
        
        alert.addPickerView(values: pickerData, initialSelection: selectedPickerData) { (vc, picker, index, value) in
            for i in index {
                switch i[0] {
                case 0:
                    let date = self.dateAndTimeInfoLabel.text!.split(separator: " ")[0]
                    self.dateAndTimeInfoLabel.text = self.dateAndTimeInfoLabel.text?.replacingOccurrences(of: date, with: pickerData[0][i[1]])
                case 1:
                    let time = self.dateAndTimeInfoLabel.text!.split(separator: " ")[1]
                    self.dateAndTimeInfoLabel.text = self.dateAndTimeInfoLabel.text?.replacingOccurrences(of: time, with: pickerData[1][i[1]])
                default: break
                }
            }
        }
        
        alert.addAction(UIAlertAction(title: "Done", style: .cancel, handler: { (action) in
            let formattedDate = self.getFormattedData(stringDate: self.dateAndTimeInfoLabel.text!)
            
                let restaurants = RestaurantDataService.shared.getRestaurantList(date: formattedDate)
                self.timeMap.removeAnnotations(self.timeMap.annotations)
                if !restaurants.isEmpty {
                    for r in restaurants {
                        if let averageTime = r.averagePickUpTime {
                            let annotation = RestaurantAnnotation(title: String(averageTime), locationName: "Id " + String(r.id), coordinate: CLLocationCoordinate2D(latitude: r.latitude, longitude: r.longitude))
                            self.timeMap.addAnnotation(annotation)
                        } else {
                            let annotation = RestaurantAnnotation(title: "No orders", locationName: "Id " + String(r.id), coordinate: CLLocationCoordinate2D(latitude: r.latitude, longitude: r.longitude))
                            self.timeMap.addAnnotation(annotation)
                        }
                    }
            }
        }) )
        alert.show()
    }
    
    func getFormattedData(stringDate: String) -> Date {
        let date = stringDate.split(separator: " ").first!
        var day = date.split(separator: ".").first!
        var month = date.split(separator: ".")[1]
        let year = date.split(separator: ".").last!
        
        let hours = self.dateAndTimeInfoLabel.text!.split(separator: " ").last!
        var hour = hours.split(separator: "-").first!
        
        day = day.count < 2 ? "0" + day : day
        month = month.count < 2 ? "0" + month : month
        hour = hour.count < 2 ? "0" + hour : hour
        
        let isoDate = "\(year)-\(month)-\(day)T\(hour):00:00Z"
        let dateFormatter = ISO8601DateFormatter()
        let formattedDate = dateFormatter.date(from:isoDate)!
        
        return formattedDate
    }
    
}
