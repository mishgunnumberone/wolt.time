//
//  DateAndTimeContainer.swift
//  Wolt.Time
//
//  Created by Mikhail Bobretsov on 15/02/2019.
//  Copyright © 2019 Mikhail Bobretsov. All rights reserved.
//

import UIKit

class DateAndTimeContainer: UIView {
    
    init() {
        super.init(frame: UIScreen.main.bounds);
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func addTopAndBottomBorders(height: CGFloat) {
        
        let topBorder = CALayer()
        let bottomBorder = CALayer()
        let borderThickness: CGFloat = 2.0
        
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.width, height: borderThickness)
        bottomBorder.frame = CGRect(x: 0.0, y: height - borderThickness, width: self.frame.width, height: borderThickness)
        topBorder.backgroundColor = WoltTimeViewController.appColor.cgColor
        bottomBorder.backgroundColor = WoltTimeViewController.appColor.cgColor
        
        self.layer.addSublayer(topBorder)
        self.layer.addSublayer(bottomBorder)
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}

